# Import GTFS Job

### Program Type:

	Batch Job

### Execution Frequency:

	N/A

### Program Description

This job imports a given GTFS zip file into a MongoDB dataset. For autonomous GTFS recording please refer to [Record GTFS Job](https://gitlab.com/cutr-at-usf/transi/core/record-gtfs) 

The program can be run independently from other services or dependent on the Data and State API Services to retrieve timezone and dataset name information.

### Program Execution (API Dependent) (Implementation Pending)

```
docker run --rm --network=my_network \
-e "DATA_URL=http://basic_api" \
-e "STATE_URL=http://state_api" \
-e "DATASET=hart" \
-e "ZIP_URL=data/HART-GTFS-2017-10-25.zip" \
-v /transi/data/zip/hart:/usr/src/app/data \
registry.gitlab.com/cutr-at-usf/transi/core/import-gtfs
```

### Execution Parameters

**DATA_URL**: URL used to access the Data API Service on docker network

**STATE_URL**: URL used to access the Logging API Service on docker network

**DATASET**: Dataset configuration name as specified in config.json for the Data API service

**ZIP_URL**: Path for GTFS package to import.

### Program Execution (Independent Runtime)

```
docker run --rm --network=my_network \
-e "DB_URL=mongodb://db:27017/hart" \
-e "TIMEZONE=America/New_York" \
-e "ZIP_URL=data/HART-GTFS-2017-10-25.zip" \
-v /transi/data/zip/hart:/usr/src/app/data \
registry.gitlab.com/cutr-at-usf/transi/core/import-gtfs
```

### Execution Parameters

**DB_URL**: URL scheme used to log into a MongoDB database.

**TIMEZONE**: Used to accurately place PB files into the correct archive.

**ZIP_URL**: Path for GTFS package to import.

Please refer to [getting started project](https://gitlab.com/cutr-at-usf/transi/core/getting-started) for more information about the system

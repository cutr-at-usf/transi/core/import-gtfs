from pymongo.errors import DuplicateKeyError
from datetime import datetime
import zipfile
import pymongo
import pytz
import json
import csv
import os

db_tables = {}
db_tables['shapes.txt'] = 'shapes'
db_tables['stop_times.txt'] = 'stop_times'
db_tables['stops.txt'] = 'stops'
db_tables['trips.txt'] = 'trips'
db_tables['agency.txt'] = 'agency'
db_tables['calendar.txt'] = 'calendar'
db_tables['calendar_dates.txt'] = 'calendar_dates'
db_tables['fare_attributes.txt'] = 'fare_attributes'
db_tables['fare_rules.txt'] = 'fare_rules'
db_tables['routes.txt'] = 'routes'
db_tables['feed_info.txt'] = 'feed_info'

db_tables['frequencies.txt'] = 'frequencies'
db_tables['transfers.txt'] = 'transfers'
db_tables['multi_route_trips.txt'] = 'multi_route_trips'
db_tables['facilities.txt'] = 'facilities'
db_tables['facilities_properties.txt'] = 'facilities_properties'
db_tables['facilities_properties_definitions.txt'] = 'facilities_properties_definitions'
db_tables['levels.txt'] = 'levels'
db_tables['pathways.txt'] = 'pathways'

db_tables['checkpoints.txt'] = 'checkpoints'
# db_tables['timepoint_times.txt'] = 'timepoint_times'
# db_tables['timepoints.txt'] = 'timepoints'

def importGTFS(conn_url, input_file, timezone):

	client = pymongo.MongoClient(conn_url)
	db = client.get_database()

	with zipfile.ZipFile(input_file) as zf:

		tz = pytz.timezone(timezone)
		zip_name_date = '-'.join(os.path.basename(input_file).rstrip('.zip').split('-')[2:])
		zip_name_timestamp = tz.localize(datetime.strptime(zip_name_date, "%Y-%m-%d"))
		zip_name_timestamp = int(zip_name_timestamp.timestamp())

		print("Date Associated: "+zip_name_date)
		
		for filename in zf.namelist():

			print("File: "+filename)
			timestamp = zip_name_timestamp

			obj = {}
			obj['header'] = { 'timestamp': timestamp }
			obj['data'] = []
			obj['data-indexed'] = {}

			with zf.open(filename, 'rU') as file:
				headers = None

				decoded = [x.decode('utf8')for x in file.readlines()]
				reader = csv.reader(decoded, delimiter=',')
				headers = next(reader)

				for line in reader:

					entry = {}

					if len(headers) != len(line):
						print("Column Mismatch. Skip Line.")
						continue

					for idx, val in enumerate(headers):
						entry[val] = line[idx]

					if filename == 'shapes.txt':

						key = entry['shape_id']

						try:
							obj['data-indexed'][key]['coordinates'].append([float(entry['shape_pt_lon']), float(entry['shape_pt_lat'])])
						except KeyError:
							shape = {}
							shape['type'] = "LineString"
							shape['properties'] = {}
							shape['properties']['shape_id'] = key
							shape['coordinates'] = [];
							shape['coordinates'].append([float(entry['shape_pt_lon']), float(entry['shape_pt_lat'])])
							obj['data-indexed'][key] = shape

					elif filename == 'stop_times.txt':

						key = entry['trip_id']
						del entry['trip_id']

						try:
							obj['data-indexed'][key]['stops'].append(entry)
						except KeyError:
							trip = {}
							trip['trip_id'] = key;
							trip['stops'] = [];
							trip['stops'].append(entry)
							obj['data-indexed'][key] = trip

					elif filename == 'stops.txt':

						stop = {}
						stop['type'] = "Point"
						stop['coordinates'] = [];								
						stop['coordinates'].append([float(entry['stop_lon']), float(entry['stop_lat'])])
						del entry['stop_lon']
						del entry['stop_lat']
						stop['properties'] = entry
						obj['data'].append(stop)

					elif filename == 'trips.txt':

						key = entry['route_id']
						del entry['route_id']

						try:
							obj['data-indexed'][key]['trips'].append(entry)
						except KeyError:
							route = {}
							route['route_id'] = key;
							route['trips'] = [];
							route['trips'].append(entry)
							obj['data-indexed'][key] = route

					elif filename in 'timepoint_times.txt':

						key = entry['trip_id']
						del entry['trip_id']

						try:
							obj['data-indexed'][key]['stops'].append(entry)
						except KeyError:
							trip = {}
							trip['trip_id'] = key;
							trip['stops'] = [];
							trip['stops'].append(entry)
							obj['data-indexed'][key] = trip

					elif filename in 'timepoints.txt':

						timepoint = {}
						timepoint['type'] = "Point"
						timepoint['coordinates'] = [];								
						timepoint['coordinates'].append([float(entry['timepoint_lon']), float(entry['timepoint_lat'])])
						del entry['timepoint_lon']
						del entry['timepoint_lat']
						stop['properties'] = entry
						obj['data'].append(stop)

					else: 

						obj['data'].append(entry)

			if filename == 'stop_times.txt':

				for idx, trip in obj['data-indexed'].items():
					trip_obj = {}
					trip_obj['header'] = {'timestamp': obj['header']['timestamp']}
					trip_obj['data'] = trip

					try:
						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(trip_obj)) )
						print("Trip "+trip['trip_id']+" from "+str(timestamp)+" inserted to DB")				
					except DuplicateKeyError:
						print("Trip "+trip['trip_id']+" from "+str(timestamp)+" rejected. Same Header")

			elif filename in 'stops.txt':

				for stop in obj['data']:
					stop_obj = {}
					stop_obj['header'] = {'timestamp': obj['header']['timestamp']}
					stop_obj['data'] = stop

					try:
						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(stop_obj)) )
						print("Stop "+stop['properties']['stop_id']+" from "+str(timestamp)+" inserted to DB")
					except DuplicateKeyError:
						print("Stop "+stop['properties']['stop_id']+" from "+str(timestamp)+" rejected. Same Header")

			elif filename in 'trips.txt':

				for idx, route in obj['data-indexed'].items():
					route_obj = {}
					route_obj['header'] = {'timestamp': obj['header']['timestamp']}
					route_obj['data'] = route

					try:
						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(route_obj)) )
						print("Route "+route['route_id']+" from "+str(timestamp)+" inserted to DB")
					except DuplicateKeyError:
						print("Route "+route['route_id']+" from "+str(timestamp)+" rejected. Same Header")

			elif filename in 'shapes.txt':

				for idx, shape in obj['data-indexed'].items():
					shape_obj = {}
					shape_obj['header'] = {'timestamp': obj['header']['timestamp']}
					shape_obj['data'] = shape

					try:
						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(shape_obj)) )
						print("Shape "+shape['properties']['shape_id']+" from "+str(timestamp)+" inserted to DB")
					except DuplicateKeyError:
						print("Shape "+shape['properties']['shape_id']+" from "+str(timestamp)+" rejected. Same Header")

#			elif filename in 'timepoint_times.txt':
#
#				for idx, trip in obj['data-indexed'].items():
#					trip_obj = {}
#					trip_obj['header'] = {'timestamp': obj['header']['timestamp']}
#					trip_obj['data'] = trip
#
#					try:
#						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(trip_obj)) )
#						print("Timepoint Trip "+trip['trip_id']+" from "+str(timestamp)+" inserted to DB")	
#					except DuplicateKeyError:
#						print("Timepoint Trip "+trip['trip_id']+" from "+str(timestamp)+" rejected. Same Header")
#
#			elif filename in 'timepoints.txt':
#
#				for timepoint in obj['data']:
#					timepoint_obj = {}
#					timepoint_obj['header'] = {'timestamp': obj['header']['timestamp']}
#					timepoint_obj['data'] = timepoint
#
#					try:
#						db[ db_tables[filename] ].insert_one( json.loads(json.dumps(timepoint_obj)) )
#						print("Timepoint "+timepoint['properties']['timepoint_id']+" from "+str(timestamp)+" inserted to DB")
#					except DuplicateKeyError:
#						print("Timepoint "+timepoint['properties']['timepoint_id']+" from "+str(timestamp)+" rejected. Same Header")

			elif filename in db_tables:

				del obj['data-indexed']
				try:
					db[ db_tables[filename] ].insert_one( json.loads(json.dumps(obj)) )
					print("Inserted New Set to DB")
				except DuplicateKeyError:
					print("New Set rejected. Same Header")

			else: 
				print("ERROR: Parse not implemented for: "+filename)
			#	exit()


	for file_name, table_name in db_tables.items():
		timestamp_index_exist = False
		if table_name in db.collection_names():
			index_info = db[table_name].index_information()
			for index in index_info:
				for key in index_info[index]['key']:
					for field in key:
						if field == "header.timestamp":
							timestamp_index_exist = True
			if not timestamp_index_exist:
				print("No Timestamp Index Located for "+table_name+". Creating...")
				if file_name == 'shapes.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.properties.shape_id", pymongo.ASCENDING)
					]
				elif file_name == 'stop_times.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.trip_id", pymongo.ASCENDING)
					]
				elif file_name == 'stops.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.properties.stop_id", pymongo.ASCENDING)
					]
				elif file_name == 'trips.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.route_id", pymongo.ASCENDING)
					]
				elif file_name in 'timepoint_times.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.trip_id", pymongo.ASCENDING)
					]
				elif file_name in 'timepoints.txt':
					index = [
						("header.timestamp", pymongo.DESCENDING),
						("data.properties.timepoint_id", pymongo.ASCENDING)
					]
				else:
					index = [("header.timestamp", pymongo.DESCENDING)]

				db[table_name].create_index(index,unique=True,background=True)

	print('Completed Executing')

if __name__ == "__main__":
	importGTFS(os.environ.get('DB_URL'), os.environ.get('ZIP_URL'), os.environ.get('TIMEZONE'))
